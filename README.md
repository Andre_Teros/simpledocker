Repo with Docker images
=======================

- [Download](#download)
- [Run Containers](#run-containers)
- [Version Description](#version-description)

# Download
clone repository
```bash
git clone https://gitlab.com/Andre_Teros/simpledocker.git
cd simpledocker/
```
and checkout version (np1.0 for example)
```bash
git checkout -b np1.0 np1.0
```


# Run Containers

start
```bash
cd path/to/SimpleDocker 
docker-compose up 
```

start after change configuration
```bash
docker-compose up --build
```
# Version Description

 - np              nginx + php
 - emp             nginx + mysql + php
 - empp            nginx + mysql + php (two versions)
